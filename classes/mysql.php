<?php

class mysql{
 
    private static $pdo; 

    public static function conectar(){
        if(self::$pdo == null){
            try{
                self::$pdo = new PDO('mysql:host='.HOST.';dbname='.DATABASE,USER,PASSWORD,array(PDO::
                MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"));
                SELF::$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            }catch(Exception $e){
                echo '<h2 style="color:red;">ERRO ao conectar com bancos de dados :(<h2>';
            }
        }
        return self::$pdo;
    }
}




?>